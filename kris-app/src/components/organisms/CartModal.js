import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { Button, Modal } from '../atoms';
import { CartProduct } from '../molecules';
import { toggleCart, setTotalPrice, setStep } from '../../store/actions';
import priceFormatter from '../../utils/formatter';
import { STEP_CHECKOUT } from '../../store/const';

const CartModal = ({ className }) => {
  const { isOpen, products, productsInCart } = useSelector(({ cart, app }) => ({
    isOpen: cart.showCart,
    productsInCart: cart.productsInCart,
    products: app.products,
  }));

  const dispatch = useDispatch();
  const history = useHistory();

  let productsAmount = 0;
  for (const key in productsInCart) {
    productsAmount += productsInCart[key];
  }

  const addedProducts = Object.keys(productsInCart).map((productInCartId) =>
    products.find((product) => product.id === productInCartId)
  );

  const totalPrice = addedProducts.reduce((acc, p) => {
    return (acc += p.price * productsInCart[p.id]);
  }, 0);

  useEffect(() => {
    dispatch(setTotalPrice(totalPrice));
  }, [totalPrice, dispatch]);

  const onCheckout = () => {
    dispatch(setTotalPrice(totalPrice));
    dispatch(toggleCart(false));
    dispatch(setStep(STEP_CHECKOUT));
    history.push('/checkout');
  };

  if (!isOpen) {
    return null;
  }

  return (
    <Modal onClose={() => dispatch(toggleCart(false))}>
      <div className={className}>
        <h3>Cart</h3>
        <small>You have {productsAmount} items in the cart</small>

        <div className="products">
          {addedProducts && addedProducts.length ? (
            addedProducts.map((p) => (
              <CartProduct amount={productsInCart[p.id]} key={p.id} {...p} />
            ))
          ) : (
            <h4>Empty</h4>
          )}
        </div>

        {addedProducts && addedProducts.length ? (
          <>
            <div className="price">
              <span>Total price:</span>
              <span>{priceFormatter.format(totalPrice)} zł</span>
            </div>

            <Button onClick={onCheckout}>Checkout</Button>
          </>
        ) : null}
      </div>
    </Modal>
  );
};

const StyledCartModal = styled(CartModal)`
  color: ${({ theme }) => theme.colors.textMediumLight};
  padding: 20px 40px;

  h3 {
    font-size: 54px;
    margin: 0 0 10px;
  }

  small,
  h4 {
    font-size: 20px;
    font-weight: ${({ theme }) => theme.fontWeight.semibold};
  }

  h4 {
    text-align: center;
    margin-top: 30px;
  }

  > .price {
    font-weight: ${({ theme }) => theme.fontWeight.semibold};
    font-size: ${({ theme }) => theme.fontSize.xl};
    color: black;
    margin-top: 40px;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  button {
    margin: 40px auto 0;
  }
`;

export default StyledCartModal;
