import React from 'react';
import styled from 'styled-components';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { InputBlock, Input, LabeledCheckbox, Button } from '../atoms';
import validate from '../../utils/validation';
import { createOrder } from '../../store/actions';

const CheckoutForm = ({ className }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { handleSubmit, getFieldProps, errors } = useFormik({
    initialValues: {
      contact: '',
      firstName: '',
      lastName: '',
      address: '',
      apartment: '',
      city: '',
      country: '',
      postalCode: '',
    },
    onSubmit: (values) => {
      dispatch(createOrder(values)).then(() => {
        history.push('/thank-you');
      });
    },
    validate,
    validateOnBlur: false,
    validateOnChange: false,
  });

  return (
    <form onSubmit={handleSubmit} className={className}>
      <span className="form-text">Contact information</span>
      <InputBlock>
        <Input
          name="contact"
          error={errors.contact}
          {...getFieldProps('contact')}
          placeholder="Email or mobile phone number"
        />
      </InputBlock>

      <LabeledCheckbox id="subscribe">
        Keep me up to date on news and exclusive offers
      </LabeledCheckbox>
      <span className="form-text">Shipping address</span>
      <InputBlock>
        <Input
          name="firstName"
          error={errors.firstName}
          {...getFieldProps('firstName')}
          placeholder="First name (optional)"
        />
        <Input
          name="lastName"
          error={errors.lastName}
          {...getFieldProps('lastName')}
          placeholder="Last name"
        />
      </InputBlock>
      <InputBlock>
        <Input
          name="address"
          error={errors.address}
          {...getFieldProps('address')}
          placeholder="Address"
        />
      </InputBlock>
      <InputBlock>
        <Input
          name="apartment"
          error={errors.apartment}
          {...getFieldProps('apartment')}
          placeholder="Apartment, suite, etc. (optional)"
        />
      </InputBlock>
      <InputBlock>
        <Input
          name="city"
          error={errors.city}
          {...getFieldProps('city')}
          placeholder="City"
        />
      </InputBlock>
      <InputBlock>
        <Input
          name="country"
          error={errors.country}
          {...getFieldProps('country')}
          placeholder="Country"
        />
        <Input
          name="postalCode"
          error={errors.postalCode}
          {...getFieldProps('postalCode')}
          placeholder="Postal code"
        />
      </InputBlock>

      {Object.keys(errors).length > 0 ? (
        <>
          {Object.keys(errors).map((e) => (
            <span className="error-message" key={e}>
              {errors[e]}
            </span>
          ))}
        </>
      ) : null}

      <Button type="submit">Place an order</Button>
    </form>
  );
};

const StyledCheckoutForm = styled(CheckoutForm)`
  width: 100%;
  margin: 0 auto;
  max-width: 590px;
  padding: 50px 10px 0;
  font-weight: ${({ theme }) => theme.fontWeight.regular};
  .form-text {
    display: block;
    font-size: ${({ theme }) => theme.fontSize.md};
    margin-bottom: 25px;
    margin-top: 45px;
  }

  label {
    margin-top: 15px;
  }

  @media (max-width: 425px) {
    padding: 10px 10px 0;
  }

  button {
    margin: 80px auto 0;

    @media (max-width: 425px) {
      margin-top: 30px;
    }
  }

  .error-message {
    border: ${({ theme }) => `2px solid ${theme.colors.borderError}`};
    border-radius: 10px;
    color: white;
    padding: 5px;
    background-color: ${({ theme }) => theme.colors.backgroundError};
    display: block;
    margin-bottom: 5px;
  }
`;

export default StyledCheckoutForm;
