import React from 'react';
import styled from 'styled-components';

import { Logo, WrappingContent as Content } from '../atoms';

const Footer = ({ className }) => (
  <footer className={`${className} container`}>
    <Content>
      <Logo />
      <span className="copyright">Copyright &copy; AppleStore</span>
    </Content>
  </footer>
);

const StyledFooter = styled(Footer)`
  background-color: ${({ theme }) => theme.colors.backgroundDark};

  @media (max-width: 425px) {
    padding: 15px 0;
    > div {
      flex-flow: column;
    }
  }

  .copyright {
    color: ${({ theme }) => theme.colors.textMediumLight};
    font-size: ${({ theme }) => theme.fontSize.lg};
    font-weight: ${({ theme }) => theme.fontWeight.semibold};

    @media (max-width: 425px) {
      width: 100%;
      text-align: center;
    }
  }

  img {
    @media (max-width: 425px) {
      width: 200px;
      display: block;
      margin: 0 auto 25px;
    }
  }
`;

export default StyledFooter;
