import React, { useState } from 'react';
import styled from 'styled-components';

import { Logo, WrappingContent as Content } from '../atoms';
import { Searchbar, Cart } from '../molecules';

const Header = ({ className }) => {
  const [isExpanded, setIsExpanded] = useState(false);
  return (
    <header className={`${className} container`}>
      <Content>
        <Logo fade={isExpanded} />
        <Searchbar
          isExpanded={isExpanded}
          onExpand={() => setIsExpanded(!isExpanded)}
        />
        <Cart />
      </Content>
    </header>
  );
};

const StyledHeader = styled(Header)`
  background-color: ${({ theme }) => theme.colors.backgroundDark};

  .searchbar {
    margin-left: auto;
  }

  .cart {
    @media (max-width: 425px) {
      margin-left: 15px;
    }
  }
`;

export default StyledHeader;
