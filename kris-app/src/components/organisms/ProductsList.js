import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Product } from '../molecules';
import { fetchProducts, addToCart } from '../../store/actions';

const ProductsList = ({ className }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    if (dispatch) {
      dispatch(fetchProducts());
    }
  }, [dispatch]);

  const { products, search } = useSelector(({ app }) => ({
    products: app.products,
    search: app.search,
  }));

  const productsToShow =
    search && search.length > 0
      ? products.filter((p) =>
          p.title.toLowerCase().includes(search.toLowerCase())
        )
      : products;

  return (
    <div className={className}>
      {productsToShow && productsToShow.length > 0 ? (
        productsToShow.map((p) => (
          <Product
            key={p.id}
            {...p}
            onAddToCart={() => {
              dispatch(addToCart(p.id));
            }}
          />
        ))
      ) : (
        <h2>Sorry, we could not find any matching products</h2>
      )}
    </div>
  );
};

const StyledProductsList = styled(ProductsList)`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(290px, 1fr));
  grid-template-rows: repeat(auto-fill, minmax(420px, auto));
  column-gap: 3rem;
  row-gap: 30px;
`;

export default StyledProductsList;
