import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Icon } from '../atoms';
import { toggleCart } from '../../store/actions';

const Cart = ({ className }) => {
  const dispatch = useDispatch();
  const { productsInCart } = useSelector(({ cart, app }) => ({
    productsInCart: cart.productsInCart,
    step: app.step,
  }));
  const onCartToggle = () => {
    dispatch(toggleCart());
  };

  let productsAmount = 0;
  for (const key in productsInCart) {
    productsAmount += productsInCart[key];
  }

  return (
    <div className={`${className} cart`} onClick={onCartToggle}>
      <Icon type="cart" />
      {productsAmount && productsAmount > 0 ? (
        <span>{productsAmount}</span>
      ) : null}
    </div>
  );
};

const StyledCart = styled(Cart)`
  margin-left: 35px;
  position: relative;
  cursor: pointer;

  img {
    width: 41px;
    height: 36px;
  }

  span {
    display: block;
    position: absolute;
    right: -5px;
    bottom: 0;
    background-color: ${({ theme }) => theme.colors.emphasised};
    color: ${({ theme }) => theme.colors.textLight};
    padding: 3px 5px;
    border-radius: 10px;
    font-size: ${({ theme }) => theme.fontSize.xs};
  }
`;

export default StyledCart;
