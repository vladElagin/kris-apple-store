import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Input, SearchIcon } from '../atoms';
import { searchCatalog } from '../../store/actions';

const Searchbar = ({ className, isExpanded, onExpand }) => {
  const search = useSelector(({ app }) => app.search);
  const dispatch = useDispatch();
  const onSearchChange = ({ target }) => dispatch(searchCatalog(target.value));

  return (
    <div className={`${className} searchbar ${isExpanded ? 'expanded' : ''}`}>
      <Input
        round
        placeholder="Search"
        value={search}
        onChange={onSearchChange}
      />
      <SearchIcon onClick={onExpand} />
    </div>
  );
};

const StyledSearbar = styled(Searchbar)`
  width: auto;
  min-width: 26px;
  max-width: 235px;
  position: relative;

  input {
    padding: 0 20px;
    width: 100%;
    visibility: hidden;
  }

  svg {
    cursor: pointer;
    position: absolute;
    top: 7px;
    right: 10px;
    width: 26px;
    height: 25px;

    .shp0 {
      stroke: white;
    }
  }

  &.expanded {
    svg {
      .shp0 {
        stroke: black;
      }
    }

    input {
      visibility: visible;
    }
  }
`;

export default StyledSearbar;
