import React from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { Icon } from '../atoms';
import priceFormatter from '../../utils/formatter';
import { removeFromCart } from '../../store/actions';

const CartProduct = ({ className, title, price, amount, id }) => {
  const dispatch = useDispatch();

  return (
    <div className={className}>
      <span className="title">{title}</span>
      <span className="price">
        {amount} &times; {priceFormatter.format(price)} zł
      </span>
      <Icon type="delete" onClick={() => dispatch(removeFromCart(id))} />
    </div>
  );
};

const StyledCartProduct = styled(CartProduct)`
  background-color: white;
  padding: 20px 45px 20px 30px;
  border-radius: 10px;
  margin-top: 30px;

  display: flex;
  align-items: center;
  justify-content: space-between;

  .title {
    font-weight: ${({ theme }) => theme.fontWeight.semibold};
    max-width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    color: black;
  }

  .price {
    margin: 0 20px 0 auto;
    white-space: nowrap;
    color: black;
    font-size: ${({ theme }) => theme.fontSize.md};
  }

  .icon {
    cursor: pointer;
  }
`;

export default StyledCartProduct;
