import React from 'react';
import styled from 'styled-components';

import { Button } from '../atoms';
import priceFormatter from '../../utils/formatter';

const Product = ({ className, title, image, price, onAddToCart }) => (
  <div className={className}>
    <img src={image} alt={title} />
    <span className="title">{title}</span>
    <div>
      <Button onClick={onAddToCart}>Add to cart</Button>
      <span>{priceFormatter.format(price)} zł</span>
    </div>
  </div>
);

const StyledProduct = styled(Product)`
  border-radius: 20px;
  background-color: white;
  font-size: ${({ theme }) => theme.fontSize.xl};
  font-weight: ${({ theme }) => theme.fontWeight.semibold};
  padding: 40px 25px 40px;

  img {
    width: 100%;
    height: 235px;
    object-fit: contain;
    margin-bottom: 10px;
  }

  .title {
    font-weight: ${({ theme }) => theme.fontWeight.bold};
    display: block;
    max-width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin-bottom: 25px;
  }

  div {
    display: flex;
    align-items: center;
    justify-content: space-between;

    span {
      margin-right: 20px;
    }

    button {
      font-size: ${({ theme }) => theme.fontSize.md};
    }

    @media (max-width: 374px) {
      flex-direction: column-reverse;

      span {
        margin-right: 0;
        margin-bottom: 10px;
      }
    }
  }
`;

export default StyledProduct;
