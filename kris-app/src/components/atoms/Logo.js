import React from 'react';
import styled from 'styled-components';

const Logo = ({ className }) => (
  <img src="logo.svg" className={className} alt="" />
);

const StyledLogo = styled(Logo)`
  width: 165px;
  transform: translateY(10px);

  @media (max-width: 425px) {
    width: ${({ fade }) => (fade ? '0' : '130px')};
    transform: none;
    margin-right: 10px;
  }
`;

export default StyledLogo;
