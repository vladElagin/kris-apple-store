import styled from 'styled-components';

const Panel = styled.div`
  background-color: white;
  border-radius: 30px;
  width: 100%;
  max-width: 920px;
  margin: 0 auto;
  padding-bottom: 85px;
`;

export default Panel;
