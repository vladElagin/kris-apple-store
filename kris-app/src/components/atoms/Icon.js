import React from 'react';
import styled from 'styled-components';

const Icon = ({ type, onClick, className }) => (
  <img
    className={`${className} icon`}
    src={`/icons/${type}.svg`}
    alt=""
    onClick={onClick || (() => null)}
  />
);

export const StyledIcon = styled(Icon)`
  background-size: contain;
`;

/** We need to render this icon inline to be able to change its color with css */
export const SearchIcon = ({ onClick }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 26 25"
    width="26"
    height="25"
    onClick={onClick}
  >
    <defs>
      <clipPath clipPathUnits="userSpaceOnUse" id="cp1">
        <path d="M-1503 -39L417 -39L417 2305L-1503 2305Z" />
      </clipPath>
    </defs>
    <style>
      {
        '.shp0 { fill: none;stroke: #000000;stroke-linecap:round;stroke-width: 3 }'
      }
    </style>
    <g id="Web 1920 – 1" clipPath="url(#cp1)">
      <g id="Group 4">
        <g id="Icon feather-search">
          <path
            id="Path 3"
            className="shp0"
            d="M20.76 11.44C20.76 16.11 16.98 19.89 12.32 19.89C7.65 19.89 3.87 16.11 3.87 11.44C3.87 6.78 7.65 3 12.32 3C16.98 3 20.76 6.78 20.76 11.44Z"
          />
          <path id="Path 4" className="shp0" d="M22.87 22L18.28 17.41" />
        </g>
      </g>
    </g>
  </svg>
);
