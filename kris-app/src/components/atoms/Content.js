import styled from 'styled-components';

const ContentBase = styled.div`
  width: 100%;
  padding: 0 15px;
  max-width: 1290px;
  margin: 0 auto;
`;

/**
 * this component shares styles for footer and header inner wrapper
 */
const WrappingContent = styled(ContentBase)`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const MainContent = styled(ContentBase)`
  padding: 170px 0;

  @media (max-width: 768px) {
    padding: 50px 15px;
  }

  @media (max-width: 425px) {
    padding: 40px 0;
  }
`;

export { ContentBase, WrappingContent, MainContent };
