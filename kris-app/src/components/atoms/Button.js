import styled from 'styled-components';

const Button = styled.button`
  color: ${({ theme }) => theme.colors.textLight};
  background-color: ${({ theme }) => theme.colors.emphasised};
  height: 34px;
  border-radius: 16px;
  padding: 0 20px;
  line-height: 34px;
  font-size: ${({ theme }) => theme.fontSize.md};
  font-weight: ${({ theme }) => theme.fontWeight.semibold};
  cursor: pointer;
  display: block;
  border: none;
`;

export default Button;
