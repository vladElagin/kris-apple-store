import React from 'react';
import styled from 'styled-components';

import { StyledIcon as Icon } from './Icon';
import Backdrop from './Backdrop';

const Modal = ({ children, className, onClose }) => (
  <>
    <div className={className}>
      <Icon type="close" onClick={onClose} />
      {children}
    </div>
    <Backdrop />
  </>
);

const StyledModal = styled(Modal)`
  z-index: 1;
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  background-color: ${({ theme }) => theme.colors.backgroundLight};

  width: 100vw;
  max-width: 650px;
  border-radius: 0 0 20px 20px;

  > .icon {
    position: absolute;
    right: 50px;
    top: 30px;
    cursor: pointer;
  }
`;

export default StyledModal;
