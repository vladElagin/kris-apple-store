import React from 'react';
import styled from 'styled-components';

const Input = ({ className, round, error, ...ownProps }) => (
  <input className={className} {...ownProps} />
);

const StyledInput = styled(Input)`
  height: ${({ round }) => (round ? '36px' : '40px')};
  border-radius: ${(props) => (props.round ? '16px' : '5px')};
  border: ${({ round, theme, error }) =>
    round
      ? 'none'
      : `1px solid ${!error ? theme.colors.border : theme.colors.borderError}`};
  font-size: ${({ theme }) => theme.fontSize.md};
  padding: 0 20px;
  :focus {
    outline: none;
  }
  ::placeholder {
    color: ${({ round, theme }) => (round ? '' : theme.colors.textMediumLight)};
  }
`;

const LabeledCheckbox = ({ className, id, children }) => (
  <label htmlFor={id} className={className}>
    <input type="checkbox" id={id} />
    {children}
  </label>
);

const StyledLabeledCheckbox = styled(LabeledCheckbox)`
  display: block;
  font-size: 14px;

  input {
    transform: scale(1.4);
    margin: 0 15px 0 5px;
  }
`;

const InputBlock = ({ className, children }) => (
  <div className={className}>{children}</div>
);

const StyledInputBlock = styled(InputBlock)`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(230px, 1fr));
  column-gap: 0.5rem;

  input {
    margin-bottom: 15px;
  }
`;

export { StyledInput, StyledLabeledCheckbox, StyledInputBlock };
