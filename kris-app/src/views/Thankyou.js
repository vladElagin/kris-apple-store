import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { MainContent, Panel, Button } from '../components/atoms';
import { clearOrder } from '../store/actions';

const Thankyou = ({ className }) => {
  const orderNumber = useSelector(({ order }) => order.orderNumber);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (!orderNumber) {
      history.push('/');
    }
  }, [orderNumber, history]);

  return (
    <main className={className}>
      <MainContent>
        <Panel>
          <p className="heading">
            Thank you for your order! <br />
            Your order number is #{orderNumber}
          </p>
          <Button
            onClick={() => {
              dispatch(clearOrder());
              history.push('/');
            }}
          >
            Back to main page
          </Button>
        </Panel>
      </MainContent>
    </main>
  );
};

const StyledThankyou = styled(Thankyou)`
  p {
    text-align: center;
    font-weight: ${({ theme }) => theme.fontWeight.semibold};
    font-size: 37px;
    color: ${({ theme }) => theme.colors.textMediumLight};
    margin: 0;
    padding-top: 80px;
  }

  button {
    display: block;
    margin: 40px auto 0;
  }
`;

export default StyledThankyou;
