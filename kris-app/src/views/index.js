import Main from './Main';
import Checkout from './Checkout';
import Thankyou from './Thankyou';

export { Main, Checkout, Thankyou };
