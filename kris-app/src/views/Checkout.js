import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { MainContent, Panel } from '../components/atoms';
import priceFormatter from '../utils/formatter';
import { STEP_CHECKOUT } from '../store/const';
import { CheckoutForm } from '../components/organisms';

const Checkout = ({ className }) => {
  const history = useHistory();
  const { currentStep, totalPrice } = useSelector(({ app, cart }) => ({
    currentStep: app.step,
    totalPrice: cart.totalPrice,
  }));

  useEffect(() => {
    if (currentStep !== STEP_CHECKOUT) {
      history.push('/');
    }
  }, [currentStep, history]);

  return (
    <main className={className}>
      <MainContent>
        <Panel>
          <div className="heading">
            <span className="title">Checkout</span>
            <span className="price">
              Total price: {priceFormatter.format(totalPrice)} zł
            </span>
          </div>

          <CheckoutForm />
        </Panel>
      </MainContent>
    </main>
  );
};

const StyledCheckout = styled(Checkout)`
  .heading {
    display: flex;
    flex-flow: row wrap;
    align-items: center;
    justify-content: space-between;
    color: ${({ theme }) => theme.colors.textMediumLight};
    padding: 40px 60px 0;

    .title {
      font-size: 42px;
    }

    .price {
      font-size: 26px;
    }
  }
`;

export default StyledCheckout;
