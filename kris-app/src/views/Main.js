import React from 'react';
import styled from 'styled-components';

import { ContentBase } from '../components/atoms';
import { ProductsList } from '../components/organisms';

const Main = ({ className }) => (
  <main className={className}>
    <ContentBase>
      <img alt="" src="wave.jpg" className="wave" />
      <ProductsList />
    </ContentBase>
  </main>
);

const StyledMain = styled(Main)`
  padding-top: 80px;
  padding-bottom: 240px;

  .wave {
    width: 100%;
    margin-bottom: 80px;
  }
`;

export default StyledMain;
