const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const phoneNumberRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
const numericRegex = /^[0-9]*$/;

const validate = ({
  contact,
  lastName,
  address,
  city,
  country,
  postalCode,
}) => {
  const errors = {};
  /**
   * Contact. Should be either phone number or email
   */
  if (!contact) {
    errors.contact = 'Contact is required';
  } else if (
    !emailRegex.test(contact.toLowerCase()) &&
    !phoneNumberRegex.test(contact.toLowerCase())
  ) {
    errors.contact = 'Contact should be either phone number of email';
  }

  /**
   * Last name. Required.
   */
  if (!lastName) {
    errors.lastName = 'Last name is required';
  }

  /**
   * Address. Required.
   */
  if (!address) {
    errors.address = 'Address is required';
  }

  /**
   * City. Required.
   */
  if (!city) {
    errors.city = 'City is required';
  }

  /**
   * Country. Required.
   */
  if (!country) {
    errors.country = 'Country is required';
  }

  /**
   * Postal code. Required.
   */
  if (!postalCode) {
    errors.postalCode = 'Postal code is required';
  } else if (!numericRegex.test(postalCode)) {
    errors.postalCode = 'Postal code should be numeric-only';
  }
  return errors;
};

export default validate;
