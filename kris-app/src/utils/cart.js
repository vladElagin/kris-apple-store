/**
 * Products that were added to cart are stored in redux store in following shape:
 * {
 *   <productId>: <added amount>
 * }
 * In order to add new product/increase amount of already added product/decrease it/remove it from cart
 * we need to perform similar actions, so I decided to cut them into separate function.
 *
 * @param {object[]} _products
 * @param {string} id
 * @param {boolean} increase - true (default) means that we are adding/increasing amount
 */
export const updateProductAmount = (_products, id, increase = true) => {
  // do deep copy of products object
  const products = { ..._products };

  // decrease/withdraw from cart
  if (!increase) {
    // if we can remove one item of product and there are one or
    // more left then update existing object
    if (products[id] - 1 > 0) {
      return {
        ...products,
        [id]: products[id] - 1,
      };
    }
    // esle remove product's key from object
    delete products[id];
    return products;
  }

  // add/increase product's amount
  return {
    ...products,
    [id]: products[id] ? products[id] + 1 : 1,
  };
};
