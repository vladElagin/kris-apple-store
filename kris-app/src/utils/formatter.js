const priceFormatter = new Intl.NumberFormat('pl-PL', {
  style: 'decimal',
  minimumFractionDigits: 2,
});

export default priceFormatter;
