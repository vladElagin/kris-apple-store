import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  body {
    margin: 0;
    font-family: Montserrat, sans-serif;
  }

  * {
    box-sizing: border-box;
  }

  .container {
    width: 100%;
  }
`;

export default GlobalStyles;
