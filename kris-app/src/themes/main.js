const mainTheme = {
  colors: {
    emphasised: '#0071e3',
    textLight: '#fff',
    textMediumLight: '#d2d2d2',
    backgroundDark: '#000',
    backgroundLight: '#f2f2f2',
    border: '#d9d9d9',
    borderError: '#de4343',
    modalBackdrop: 'rgba(0, 0, 0, 0.7)',
    backgroundError: '#de8585',
  },
  fontWeight: {
    regular: 400,
    semibold: 600,
    bold: 700,
  },
  fontSize: {
    xs: '11px',
    sm: '15px',
    md: '17px',
    lg: '20px',
    xl: '22px',
  },
};

export default mainTheme;
