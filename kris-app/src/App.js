import React from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';

import GlobalStyles from './themes/globalStyles';
import theme from './themes/main';
import { Header, Footer, CartModal } from './components/organisms';
import { Main, Thankyou, Checkout } from './views';
import store from './store';

const App = ({ className }) => {
  return (
    <Provider store={store}>
      <GlobalStyles />
      <ThemeProvider theme={theme}>
        <Router>
          <div className={className}>
            <Header />
            <CartModal />
            <Route path="/" exact component={Main} />
            <Route path="/checkout" component={Checkout} />
            <Route path="/thank-you" component={Thankyou} />
            <Footer />
          </div>
        </Router>
      </ThemeProvider>
    </Provider>
  );
};

const StyledApp = styled(App)`
  background-color: ${theme.colors.backgroundLight};
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: minmax(100px, auto) minmax(calc(100vh - 160px), auto) minmax(
      100px,
      auto
    );
  grid-template-areas:
    'header'
    'main'
    'footer';
`;

export default StyledApp;
