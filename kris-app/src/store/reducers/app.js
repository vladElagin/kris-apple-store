import { SEARCH_VALUE_CHANGE, SET_PRODUCTS } from '../actions';
import { STEP_COLLECTING } from '../const';
import { SET_STEP } from '../actions';

const initialState = {
  search: '',
  products: [],
  step: STEP_COLLECTING,
};

const appReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_PRODUCTS:
      return {
        ...state,
        products: [...payload],
      };

    case SEARCH_VALUE_CHANGE:
      return {
        ...state,
        search: payload,
      };

    case SET_STEP:
      return {
        ...state,
        step: payload,
      };

    default:
      return state;
  }
};

export default appReducer;
