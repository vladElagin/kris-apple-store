import { ORDER_CREATED, ORDER_CLEAR } from '../actions';

const initialState = {
  orderNumber: null,
};

const orderReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ORDER_CREATED:
      return {
        ...state,
        orderNumber: payload,
      };

    case ORDER_CLEAR:
      return {
        ...state,
        orderNumber: null,
      };

    default:
      return state;
  }
};

export default orderReducer;
