import { combineReducers } from 'redux';

import appReducer from './app';
import cartReducer from './cart';
import orderReducer from './order';

const rootReducer = combineReducers({
  app: appReducer,
  cart: cartReducer,
  order: orderReducer,
});

export default rootReducer;
