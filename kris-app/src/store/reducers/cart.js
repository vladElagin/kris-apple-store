import {
  ADD_TO_CART,
  TOGGLE_CART,
  CLEAR_CART,
  REMOVE_FROM_CART,
  SET_TOTAL_PRICE,
} from '../actions';

import { updateProductAmount } from '../../utils/cart';

const initialState = {
  showCart: false,
  productsInCart: {},
  totalPrice: 0,
};

const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART:
      return {
        ...state,
        productsInCart: updateProductAmount(state.productsInCart, payload),
      };

    case REMOVE_FROM_CART:
      return {
        ...state,
        productsInCart: updateProductAmount(
          state.productsInCart,
          payload,
          false
        ),
      };

    case TOGGLE_CART:
      return {
        ...state,
        showCart: payload || !state.showCart,
      };

    case SET_TOTAL_PRICE:
      return {
        ...state,
        totalPrice: payload,
      };

    case CLEAR_CART:
      return {
        showCart: initialState.showCart,
        productsInCart: initialState.productsInCart,
      };

    default:
      return state;
  }
};

export default cartReducer;
