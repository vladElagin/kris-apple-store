import axios from 'axios';
import { BASE_URL } from '../const';
import { clearCart } from './cart';

export const ORDER_CREATED = 'ORDER_CREATED';
export const ORDER_CLEAR = 'ORDER_CLEAR';

const setOrderCreated = (orderNumber) => ({
  type: ORDER_CREATED,
  payload: orderNumber,
});

export const createOrder = (orderData) => {
  return (dispatch, getState) => {
    const { cart } = getState();
    const data = {
      ...orderData,
      orderNumber: new Date().valueOf().toString(),
      totalPrice: cart.totalPrice,
      products: Object.keys(cart.productsInCart).map((prodId) => ({
        product: prodId,
        amount: cart.productsInCart[prodId],
      })),
    };

    return axios.post(`${BASE_URL}/orders`, data).then(({ status, data }) => {
      if (status !== 200) {
        throw new Error("Couldn't create order");
      }

      dispatch(setOrderCreated(data.orderNumber));
      dispatch(clearCart());
      return Promise.resolve();
    });
  };
};

export const clearOrder = () => ({ type: ORDER_CLEAR });
