export const TOGGLE_CART = 'TOGGLE_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const CLEAR_CART = 'CLEAR_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const SET_TOTAL_PRICE = 'SET_TOTAL_PRICE';

export const addToCart = (payload) => {
  return { type: ADD_TO_CART, payload };
};

export const toggleCart = (payload) => {
  return { type: TOGGLE_CART, payload };
};

export const clearCart = (payload) => {
  return { type: CLEAR_CART, payload };
};

export const removeFromCart = (payload) => {
  return { type: REMOVE_FROM_CART, payload };
};

export const setTotalPrice = (payload) => {
  return { type: SET_TOTAL_PRICE, payload };
};
