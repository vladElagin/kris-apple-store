import axios from 'axios';

import { BASE_URL } from '../const';

export const SET_PRODUCTS = 'SET_PRODUCTS';
export const SEARCH_VALUE_CHANGE = 'SEARCH_VALUE_CHANGE';
export const SET_STEP = 'SET_STEP';

export const fetchProductsSuccess = (payload) => {
  return { type: SET_PRODUCTS, payload };
};

export const fetchProducts = () => {
  return (dispatch) => {
    return axios
      .get(`${BASE_URL}/products`)
      .then(({ data }) => {
        const products = data.map(({ id, title, image, price }) => ({
          id,
          image: image.url,
          title,
          price: price.toFixed(2),
        }));

        dispatch(fetchProductsSuccess(products));
      })
      .catch((error) => {
        throw error;
      });
  };
};

export const searchCatalog = (payload) => {
  return { type: SEARCH_VALUE_CHANGE, payload };
};

export const setStep = (payload) => {
  return { type: SET_STEP, payload };
};
