export const BASE_URL = 'https://kris-app.herokuapp.com';

export const STEP_COLLECTING = 'STEP_COLLECTING';
export const STEP_CHECKOUT = 'STEP_CHECKOUT';
export const STEP_COMPLETED = 'STEP_COMPLETED';
